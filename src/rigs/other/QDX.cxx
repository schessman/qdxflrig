// ----------------------------------------------------------------------------
// Copyright (C) 2022
//              Samuel Chessman, W4SSC
//              based on QCXPlus by David Freese, W1HKJ
//
// This file is part of flrig.
//
// flrig is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// flrig is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// aunsigned long int with this program.  If not, see <http://www.gnu.org/licenses/>.
// ----------------------------------------------------------------------------

#include "other/QDX.h"
#include "support.h"

static const char QDXname_[] = "QDX";

// modes 1 and 3 are used.  2 is placeholder.
static const char *QDXmodes_[] = {
		"LSB", "X", "USB", NULL};
static const char QDX_mode_chr[] =  { '1', '2', '3' };
static const char QDX_mode_type[] = { 'L', 'X', 'U' };

static const char *QDX_USBwidths[] = {
"3200", NULL};
static int QDX_USB_bw_vals[] = {1, WVALS_LIMIT};

static GUI rig_widgets[]= {
	{ (Fl_Widget *)NULL,          0,   0,   0 }
};

RIG_QDX::RIG_QDX() {
// base class values
	name_ = QDXname_;
	modes_ = QDXmodes_;
	_mode_type = QDX_mode_type;
	bandwidths_ = QDX_USBwidths;
	bw_vals_ = QDX_USB_bw_vals;

	widgets = rig_widgets;

	comm_baudrate = BR38400;
	stopbits = 1;
	comm_retries = 1;
	comm_wait = 5;
	comm_timeout = 50;
	comm_rtscts  = false;
	comm_rtsplus = false;
	comm_dtrplus = false;
	comm_catptt  = false;
	comm_rtsptt  = false;
	comm_dtrptt  = false;
	B.imode = A.imode = 1;
	A.freq = 7040000;
	B.freq = 7040000;

	has_extras = false;

	has_noise_reduction =
	has_noise_reduction_control =
	has_auto_notch =
	has_noise_control =
	has_smeter = 
	has_bandwidth_control =
	has_sql_control =
	can_synch_clock = false;

	has_split = true;

	has_mode_control = true;

	has_ptt_control = true;

	precision = 1;
	ndigits = 8;

}

void RIG_QDX::initialize()
{
	sendCommand("TQ0;"); // receive state
	return;
}

/*
========================================================================
	frequency & mode data are contained in the IF; response

    01234567890123456789012345678901234567
    IF00014074000     +00000000003100000 ;
	IFggmmmkkkhhh     snnnnrxmyytdfcpzz ;    38 characters including the ;

	gg~~hhh = Value as defined in FA Command
	s = "+" or "-" sign of RIT
	nnnn = Value of RIT (nnnn Hz.)
	r = RIT status 0 = RIT OFF, 1 = RIT ON
	x = Always 0 QDX has no XIT
	m = Always 0 Memory channel bank number
	yy = Always 00 Memory Channel No.
	t = 0 for RX 1 for TX
	d = Operating Mode 1 = LSB, 3 = USB
	f = Receive VFO: 0 = VFO A, 1 = VFO B
	c = Always 0 scan status
	p = Split: 0 = Simplex operation (VFO mode A or VFO mode B), 1 = Split
	zz = Always 0 Tone, Tone number
========================================================================
*/
int RIG_QDX::get_IF()
{
	cmd = "IF;"; // Get transceiver information (TS-480 format)
  	int ret = wait_char(';', 38, 100, "get IF", ASC);
	gett("");
	LOG_DEBUG ("get_IF: read %d bytes, %s", ret, replystr.c_str());
	return ret;
}

void RIG_QDX::set_modeA(int val)
{
	showresp(WARN, ASC, "set mode A", "", "");
	modeA = val;
	cmd = "MD"; //  Get/Set operating mode, 1=LSB, 3=USB
	cmd += QDX_mode_chr[val];
	cmd += ';';
	set_trace(1, "set modeA");
	sendCommand(cmd);
	sett("");
}

int RIG_QDX::get_modeA()
{
	int ret = get_IF();
	if (ret < 38) return modeA;
	size_t p = replystr.rfind("IF");
	if (p == std::string::npos) return modeA;
	modeA = replystr[p+29] - '1'; // 0 - 2
	return modeA;
}

void RIG_QDX::set_modeB(int val)
{
	showresp(WARN, ASC, "set mode B", "", "");
	modeB = val;
	cmd = "MD"; //  Get/Set operating mode, 1=LSB, 3=USB
	cmd += QDX_mode_chr[val];
	cmd += ';';
	set_trace(1, "set modeB");
	sendCommand(cmd);
	sett("");
}

int RIG_QDX::get_modeB()
{
	int ret = get_IF();
	if (ret < 38) return modeB;
	size_t p = replystr.rfind("IF");
	if (p == std::string::npos) return modeB;
	modeB = replystr[p+29] - '1'; // 0 - 2
	return modeB;
}

int RIG_QDX::get_modetype(int n)
{
	return _mode_type[n];
}

void RIG_QDX::set_bwA(int val)
{
	A.iBW = 0;
}

int RIG_QDX::get_bwA()
{
	return A.iBW = 0;
}

void RIG_QDX::set_bwB(int val)
{
	B.iBW = 0;
}

int RIG_QDX::get_bwB()
{
	return B.iBW = 0;
}

// Tranceiver PTT on/off
int RIG_QDX::get_PTT()
{
	get_trace(1, "get PTT");
	int ret = get_IF();
	if (ret < 38) return ptt_;
	ptt_ = (replystr[25] == '1');
	return ptt_;
}

// Tranceiver PTT on/off
void RIG_QDX::set_PTT_control(int val)
{
	set_trace(1, "set PTT");
	if (val) sendCommand("TX;");
	else	 sendCommand("RX;");
	sett("");
	ptt_ = val;
}


void RIG_QDX::set_split(bool val) 
{
	split = val;
	if (inuse == onB) {
		if (val) {
			cmd = "FR1;FT0;";
			sendCommand(cmd);
			showresp(WARN, ASC, "Rx on B, Tx on A", cmd, "");
		} else {
			cmd = "FR1;FT1;";
			sendCommand(cmd);
			showresp(WARN, ASC, "Rx on B, Tx on B", cmd, "");
		}
	} else {
		if (val) {
			cmd = "FR0;FT1;";
			sendCommand(cmd);
			showresp(WARN, ASC, "Rx on A, Tx on B", cmd, "");
		} else {
			cmd = "FR0;FT0;";
			sendCommand(cmd);
			showresp(WARN, ASC, "Rx on A, Tx on A", cmd, "");
		}
	}
}

bool RIG_QDX::can_split()
{
	return true;
}

int RIG_QDX::get_split()
{
	size_t p;
	int split = 0;
	char rx = 0, tx = 0;
// tx vfo
	cmd = rsp = "FT";
	cmd.append(";");
	if (wait_char(';', 4, 100, "get split tx vfo", ASC) == 4) {
		p = replystr.rfind(rsp);
		if (p == std::string::npos) return split;
		tx = replystr[p+2];
	}
// rx vfo
	cmd = rsp = "FR";
	cmd.append(";");
	if (wait_char(';', 4, 100, "get split rx vfo", ASC) == 4) {
		p = replystr.rfind(rsp);
		if (p == std::string::npos) return split;
		rx = replystr[p+2];
	}

	if (tx == '0' && rx == '0') split = 0;
	else if (tx == '1' && rx == '0') split = 1;
	else if (tx == '0' && rx == '1') split = 2;
	else if (tx == '1' && rx == '1') split = 3;

	return split;
}

unsigned long int RIG_QDX::get_vfoA ()
{
	cmd = "FA;";
	get_trace(1, "get_vfoA()");
	if (wait_char(';', 14, 100, "get vfo A", ASC) < 14) return A.freq;
	gett("vfoA:");

	size_t p = replystr.rfind("FA");
	if (p != std::string::npos && (p + 12 < replystr.length())) {
		int f = 0;
		for (size_t n = 2; n < 13; n++)
			f = f*10 + replystr[p+n] - '0';
		A.freq = f;
	}
	return A.freq;
}

void RIG_QDX::set_vfoA (unsigned long int freq)
{
	A.freq = freq;
	cmd = "FA00000000000;";
	for (int i = 12; i > 1; i--) {
		cmd[i] += freq % 10;
		freq /= 10;
	}
	sendCommand(cmd);
	showresp(WARN, ASC, "set vfo A", cmd, "");
}

unsigned long int RIG_QDX::get_vfoB ()
{
	cmd = "FB;";
	get_trace(1, "get_vfoB()");
	if (wait_char(';', 14, 100, "get vfo B", ASC) < 14) return B.freq;
	gett("vfoB:");

	size_t p = replystr.rfind("FB");
	if (p != std::string::npos && (p + 12 < replystr.length())) {
		int f = 0;
		for (size_t n = 2; n < 13; n++)
			f = f*10 + replystr[p+n] - '0';
		B.freq = f;
	}
	return B.freq;
}

void RIG_QDX::set_vfoB (unsigned long int freq)
{
	B.freq = freq;
	cmd = "FB00000000000;";
	for (int i = 12; i > 1; i--) {
		cmd[i] += freq % 10;
		freq /= 10;
	}
	sendCommand(cmd);
	showresp(WARN, ASC, "set vfo B", cmd, "");
}

void RIG_QDX::selectA()
{
	cmd = "FR0;";
	sendCommand(cmd);
	cmd = "FT0;";
	sendCommand(cmd);
	inuse = onA;
}

void RIG_QDX::selectB()
{
	cmd = "FR1;";
	sendCommand(cmd);
	cmd = "FT1;";
	sendCommand(cmd);
	inuse = onB;
}
